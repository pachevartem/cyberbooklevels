%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: RoverRootMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Marsohod_antenns
    m_Weight: 0
  - m_Path: Marsohod_back_leg
    m_Weight: 0
  - m_Path: Marsohod_back_leg1
    m_Weight: 0
  - m_Path: Marsohod_body
    m_Weight: 0
  - m_Path: Marsohod_front_leg
    m_Weight: 0
  - m_Path: Marsohod_front_leg1
    m_Weight: 0
  - m_Path: Marsohod_hand
    m_Weight: 0
  - m_Path: Marsohod_hand1
    m_Weight: 0
  - m_Path: Marsohod_head
    m_Weight: 0
  - m_Path: Marsohod_pews
    m_Weight: 0
  - m_Path: root
    m_Weight: 1
  - m_Path: root/body
    m_Weight: 0
  - m_Path: root/body/arm_L
    m_Weight: 0
  - m_Path: root/body/arm_L/hand_L
    m_Weight: 0
  - m_Path: root/body/arm_L/hand_L/hand_end_L
    m_Weight: 0
  - m_Path: root/body/arm_R
    m_Weight: 0
  - m_Path: root/body/arm_R/hand_R
    m_Weight: 0
  - m_Path: root/body/arm_R/hand_R/hand_end_R
    m_Weight: 0
  - m_Path: root/body/manipulator_L
    m_Weight: 0
  - m_Path: root/body/manipulator_L/manipulator_end_L
    m_Weight: 0
  - m_Path: root/body/manipulator_R
    m_Weight: 0
  - m_Path: root/body/manipulator_R/manipulator_end_R
    m_Weight: 0
  - m_Path: root/body/neck
    m_Weight: 0
  - m_Path: root/body/neck/neck1
    m_Weight: 0
  - m_Path: root/body/neck/neck1/head
    m_Weight: 0
  - m_Path: root/body/neck/neck1/head/eye
    m_Weight: 0
  - m_Path: root/body/pew_L
    m_Weight: 0
  - m_Path: root/body/pew_L/pew_end_L
    m_Weight: 0
  - m_Path: root/body/pew_R
    m_Weight: 0
  - m_Path: root/body/pew_R/pew_end_R
    m_Weight: 0
  - m_Path: root/body/wheel_LB
    m_Weight: 0
  - m_Path: root/body/wheel_LB/wheel_end_LB
    m_Weight: 0
  - m_Path: root/body/wheel_LF
    m_Weight: 0
  - m_Path: root/body/wheel_LF/wheel_end_LF
    m_Weight: 0
  - m_Path: root/body/wheel_RB
    m_Weight: 0
  - m_Path: root/body/wheel_RB/wheel_end_RB
    m_Weight: 0
  - m_Path: root/body/wheel_RF
    m_Weight: 0
  - m_Path: root/body/wheel_RF/wheel_end_RF
    m_Weight: 0
